#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <string.h>

#include "syntree.h"

extern int syntreeInit(Syntree *self) {
    self->nodes = (Node*) NULL;
    self->count = 0;
    return 0;
}

extern void syntreeRelease(Syntree *self) {
    for (int i = 0; i < self->count; i++) {
        Node node = self->nodes[i];
        if (node.type == NODE) {
            free(node.value.children.head);
        }
    }
    
    free(self->nodes);
}

Node* getNodePointerByID(const Syntree *self, SyntreeNodeID id) {
    return &(self->nodes[id - 1]);
}

SyntreeNodeID addNodeToSyntree(Syntree *self, Node node) {
    self->count += 1;
    self->nodes = (struct Node*) realloc(self->nodes, self->count * sizeof(struct Node));
    assert(self->nodes != NULL);
    self->nodes[self->count - 1] = node;

    return self->count;
}

extern SyntreeNodeID syntreeNodeNumber(Syntree *self, int number) {
    Node newNode;
    newNode.type = LEAF;
    newNode.value.number = number;

    SyntreeNodeID id = addNodeToSyntree(self, newNode);
    return id;
}

extern SyntreeNodeID syntreeNodeTag(Syntree *self, SyntreeNodeID nodeId) {
    Node newNode;
    newNode.type = NODE;
    SyntreeNodeID* children = malloc(sizeof(SyntreeNodeID));
    assert(children != NULL);
    children[0] = nodeId;
    newNode.value.children.count = 1;
    newNode.value.children.head = children;

    SyntreeNodeID id = addNodeToSyntree(self, newNode);
    return id;
}


extern SyntreeNodeID syntreeNodePair(Syntree *self, SyntreeNodeID id1, SyntreeNodeID id2) {
    Node newNode;
    newNode.type = NODE;
    SyntreeNodeID* children = malloc(2 * sizeof(SyntreeNodeID));
    assert(children != NULL);
    children[0] = id1;
    children[1] = id2;
    newNode.value.children.count = 2;
    newNode.value.children.head = children;

    SyntreeNodeID id = addNodeToSyntree(self, newNode);
    return id;
}

extern SyntreeNodeID syntreeNodeAppend(Syntree *self, SyntreeNodeID list, SyntreeNodeID elem) {
    Node* listNode = getNodePointerByID(self, list);
    assert(listNode->type == NODE);

    listNode->value.children.count += 1;
    listNode->value.children.head = (SyntreeNodeID*) realloc(
        listNode->value.children.head,
        listNode->value.children.count * sizeof(SyntreeNodeID)
    );

    assert(listNode->value.children.head != NULL);

    listNode->value.children.head[listNode->value.children.count - 1] = elem;

    return list;
}

extern SyntreeNodeID syntreeNodePrepend(Syntree *self, SyntreeNodeID elem, SyntreeNodeID list) {
    Node* listNode = getNodePointerByID(self, list);
    assert(listNode->type == NODE);

    listNode->value.children.count += 1;
    listNode->value.children.head = (SyntreeNodeID*) realloc(
        listNode->value.children.head,
        listNode->value.children.count * sizeof(SyntreeNodeID)
    );
    
    assert(listNode->value.children.head != NULL);

    int count = listNode->value.children.count;
    for (int i = count - 1; i > 0; i--) {
        listNode->value.children.head[i] = listNode->value.children.head[i - 1];
    }

    listNode->value.children.head[0] = elem;

    return list;
}

void printSpaces(int depth) {
    for (int i = 0; i < depth; i++) {
        printf("%s", "    ");
    }
}

void syntreePrintWithDepth(const Syntree *self, SyntreeNodeID root, int depth) {
    Node rootNode = *getNodePointerByID(self, root);
    if (rootNode.type == LEAF) {
        printSpaces(depth);
        int number = rootNode.value.number;
        printf("(%i)\n", number);
        return;
    }

    printSpaces(depth);
    printf("%s", "{\n");
    for (int i = 0; i < rootNode.value.children.count; i++) {
        syntreePrintWithDepth(self, rootNode.value.children.head[i], depth + 1);

    }

    printSpaces(depth);
    printf("%s", "}\n");
}

extern void syntreePrint(const Syntree *self, SyntreeNodeID root) {
    syntreePrintWithDepth(self, root, 0);
}
